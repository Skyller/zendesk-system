@extends('layouts.app')

@section('content')
<div class="text-center">
            <div class="links"><br><br>
                    <a href="">#Nothing is here!!</a>
                    <a href="">#Congratulations!!</a>
                    <a href="">#You just received a big present!!</a>
                    <a href="">#Here is your zendesk!!</a>
                </div><br>
            <div class="content">
                <div class="title m-b-md">
                    <h5>Hey, guys<br>Congratulations,<br>You successfully login to yours own account</h5><br>
            <h5>Contact us to learn about tongue twisters<br>
            Follow our Youtube channel "Hiero ZenZen"<h5>
        </div>
</div>

<script>
var waitForZopim = setInterval(function () {
    if (window.$zopim === undefined || window.$zopim.livechat === undefined) {
        return;
    }
    $zopim(function() {
        $zopim.livechat.setName("{{\Auth::user()->name}}");
        $zopim.livechat.setEmail("{{\Auth::user()->email}}");
    });
    clearInterval(waitForZopim);
}, 100);
</script>
@endsection